function addTextField(id){
    var colors = new Array('#660000','#33ff00','#0066ff','#cc3399','#9966ff');
    var container = document.getElementById(id);
    var ulElement = document.createElement('ul');
    container.appendChild(ulElement);
    var hideLink = function(){
        var firstElement = ulElement.firstChild.getElementsByTagName('a')[0];
            firstElement.style.display = (ulElement.childNodes.length==1)?'none':'inline';
        for(var i = 0 ; i <ulElement.childNodes.length; i++)
            ulElement.childNodes[i].style.color = colors[i%5];
    }
    var addListElement = function(){
        var liElement = document.createElement('li');
        ulElement.appendChild(liElement);
        var textElement = document.createElement('input');
        textElement.setAttribute('type','text');
        liElement.appendChild(textElement);

        var deleteLink = document.createElement('a');
        deleteLink.href = "#";
  var icon = document.createElement('i');
  icon.classList.add('fa');
  icon.classList.add('fa-trash');
        deleteLink.appendChild(icon);
        liElement.appendChild(deleteLink);
        deleteLink.onclick = function(){
            ulElement.removeChild(liElement);
            hideLink();
        }
        hideLink();
    }
    addListElement();
    var anchorElement = document.createElement('a');
    anchorElement.href = "#";
    anchorElement.appendChild(document.createTextNode('Add more'));
    container.appendChild(anchorElement);
    anchorElement.onclick = addListElement;
    hideLink();
            
}
addTextField('container');