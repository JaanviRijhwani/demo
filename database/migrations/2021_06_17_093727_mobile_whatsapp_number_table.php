<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MobileWhatsappNumberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mobile_number_whatsapp_numbers', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('mobile_number');
            $table->unsignedBigInteger('is_primary_mobile');
            $table->unsignedBigInteger('is_primary_wtsp');
            $table->unsignedBigInteger('is_whatsapp');
            $table->unsignedBigInteger('employee_id');
            $table->timestamps();

            $table->foreign('employee_id')
            ->references('id')
            ->on('employees')
            ->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
