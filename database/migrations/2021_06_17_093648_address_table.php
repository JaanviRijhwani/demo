<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->id();
            $table->text('address 1');
            $table->text('address 2');
            $table->string('location');
            $table->unsignedBigInteger('zip');
            $table->string('postal area');
            $table->string('taluka');
            $table->string('suburb');
            $table->string('east/west');
            $table->string('city');
            $table->string('district');
            $table->string('state');
            $table->string('country');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
