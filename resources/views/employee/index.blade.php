@extends('layouts.app')
@section('page-level-styles')
    <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css"> -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5.15.3/css/fontawesome.min.css">
    <!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
@endsection
@section('content')
<div class="container">
        <div class="row">
            <div class="col-md-12">
                
                <div class="d-flex justify-content-end mb-3 bg-back">
                    <a href="{{ route('employee.create') }}" class="btn btn-primary">Create Employee</a>
                </div>
            </div>
        </div>
        <table class="table table-striped" id="team-table">
                <thead>
                    <th>Name</th>
                    <th>City</th>
                    <th>State</th>
                    <th>Actions</th>
                    
                </thead>
                <tbody>
                    @foreach($employees as $emp)
                    <tr >
                        <td>{{$emp->name}}</td> 
                        <td>{{$emp->city}}</td>
                        <td>{{$emp->state}}</td>
                        <td>
                                <a class="btn btn-primary btn-sm text-white" href="{{route('employee.edit', $emp->id)}}" ><i class="fa fa-pen"></i></a>
                                <a href="" class="btn btn-danger btn-sm" onclick="displayModalDelete({{$emp->id}})" data-toggle="modal" data-target="#deleteModal"><i class="fa fa-trash"></i></a>
                                <a class="btn btn-success btn-sm text-white" href=""><i class="fa fa-eye"></i></a>
                                </td>
                    </tr>
                    @endforeach
                    
                </tbody>
            </table>
            <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            
                <form action="" method="POST" id="deleteForm">
                    @csrf
                    @method('DELETE')
                <div class="modal-body">
                    <p>Are you sure want to delete Employee?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger">Delete Employee</button>
                </div>
          </div>
        </form>
        </div>
      </div>

</div>


@endsection

@section('page-level-styles')
    <style>
        .a-tag {
            text-decoration:none !important
        }
    </style>
@endsection
@section('page-level-scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous"></script>
<!--     <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
    <script src="//cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready( function () {
            $('#team-table').DataTable({
                "lengthMenu": [5, 10, 20, "All"],
                "order": [
                    [1, "ASC"]
                ],
                "columnDefs": [{
                    'orderable': false,
                    'targets': [-1]
                }]
            });
        }); 

        
    </script> -->
    <script src="https://use.fontawesome.com/releases/v5.15.3/js/all.js" data-auto-replace-svg="nest"></script>

    <script type="text/javascript">
        function displayModalDelete($id) {
            console.log($id);
            var url = 'employee/' + $id;
            $("#deleteForm").attr('action', url);
        }
    </script>
@endsection