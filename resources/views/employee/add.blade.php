@extends('layouts.app')
@section('page-level-styles')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5.15.3/css/fontawesome.min.css">
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><h3>Add Employee</h3></div>
                        <div class="card-body">
                            <form action="{{ route('employee.store') }}" method="POST">
                                @csrf
                                <div class="form-group">
                                    <label for="name">Employee Name</label>
                                    <input type="text"
                                        id="name"
                                        name="name"
                                        class="fom-control"
                                        >
                                    @error('name')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <h4>Address Details</h4>
                                <div class="row">
                                <div class="col">
                                <div class="form-group">
                                    <label for="address1">Address 1</label>
                                    <input type="text"
                                        id="address1"
                                        name="address1"
                                        class="fom-control"
                                        >
                                    @error('address1')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                </div>
                                <div class="col">
                                <div class="form-group">
                                    <label for="address2">Address 2</label>
                                    <input type="text"
                                        id="address2"
                                        name="address2"
                                        class="fom-control"
                                        >
                                    @error('address2')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                </div>

                                <div class="col">
                                <div class="form-group">
                                    <label for="location">Location</label>
                                    <input type="text"
                                        id="location"
                                        name="location"
                                        class="fom-control"
                                        >
                                    @error('location')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                </div>
                                </div>

                                <div class="row">
                                <div class="col">
                                <div class="form-group">
                                    <label for="zip">Zip/Postal Code</label>
                                    <input type="text"
                                        id="zip"
                                        name="zip"
                                        class="fom-control"
                                        >
                                    @error('zip')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                </div>
                                <div class="col">
                                <div class="form-group">
                                    <label for="postal_area">Postal Area</label>
                                    <input type="text"
                                        id="postal_area"
                                        name="postal_area"
                                        class="fom-control"
                                        >
                                    @error('postal_area')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                </div>

                                <div class="col">
                                <div class="form-group">
                                    <label for="taluka">Taluka</label>
                                    <input type="text"
                                        id="taluka"
                                        name="taluka"
                                        class="fom-control"
                                        >
                                    @error('taluka')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                </div>
                                </div>


                                <div class="row">
                                <div class="col">
                                <div class="form-group">
                                    <label for="suburb">Suburb</label>
                                    <input type="text"
                                        id="suburb"
                                        name="suburb"
                                        class="fom-control"
                                        >
                                    @error('suburb')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                </div>
                                <div class="col">
                                <div class="form-group">
                                    <label for="east_west">East/West</label>
                                    <input type="text"
                                        id="east_west"
                                        name="east_west"
                                        class="fom-control"
                                        >
                                    @error('east_west')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                </div>

                                <div class="col">
                                <div class="form-group">
                                    <label for="city">City</label>
                                    <input type="text"
                                        id="city"
                                        name="city"
                                        class="fom-control"
                                        >
                                    @error('city')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                </div>
                                </div>

                                <div class="row">
                                <div class="col">
                                <div class="form-group">
                                    <label for="district">District</label>
                                    <input type="text"
                                        id="district"
                                        name="district"
                                        class="fom-control"
                                        >
                                    @error('district')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                </div>
                                <div class="col">
                                <div class="form-group">
                                    <label for="state">State</label>
                                    <input type="text"
                                        id="state"
                                        name="state"
                                        class="fom-control"
                                        >
                                    @error('state')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                </div>

                                <div class="col">
                                <div class="form-group">
                                    <label for="country">Country</label>
                                    <input type="text"
                                        id="country"
                                        name="country"
                                        class="fom-control"
                                        >
                                    @error('country')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                </div>
                                </div>

                                <h4>Contact Details</h4>
                                <div class="row">
                                <div class="col">
                            <div class="row">
                                <div class="col" id="contact">
                                    <label for="contact1">Contact Number</label>
                                    <input type="text"  id="contact1" name="contact[]"  class="c-secondary f-22 validate form-control" >
                                    <input type="radio" name = "radio" id = "contact1" value = "contact1" class="radio" onclick = "checked('contact1')"> Primary                                
                                </div>
                                <div class="col ">
                                    <a class="btn-floating btn btn-primary pulse plus" id="add-button-email" onclick ="addContactTextField('contact')">+</a>
                                </div>
                            </div>
                        </div>
                                
                                
                                <div class="col">
                            <div class="row">
                                <div class="col" id="whatsapp">
                                    <label for="whatsapp1">WhatsApp Number</label>
                                    <input type="text" id="whatsapp1" name="whatsapp[]"  class="c-secondary f-22 validate form-control" id="whatsapp1">
                                    <input type="radio" name = "wtsp_radio" id = "whatsapp1" value = "whatsapp1" class="radio" onclick = "checked('whatsapp1')"> Primary                                
                                </div>
                                
                                <div class="col ">
                                    <a class="btn-floating btn btn-primary pulse plus" onclick ="addWhatsappTextField('whatsapp')" id="add-button-email">+</a>
                                </div>
                            </div>
                        

                            </div>
                        <div class="col ">
                            <div class="row">
                                <div class="col s10" id = "email">
                                    <label for="email1">Email Address</label>
                                    <input type="text" id="email1" name="email[]"  class="c-secondary f-22 validate form-control" id="email">
                                    <input type="radio" name = "email_radio" id = "email1" value = "email1" class="radio" onclick = "checked('email1')"> Primary                                
                                </div>
                                <div class="col s2">
                                    <a class="btn-floating btn btn-primary pulse plus" id="add-button-email" onclick = "addEmailTextField('email')">+</a>
                                </div>
                            </div>
                        </div>
                                </div>
                                </div>

                                
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary" onclick="setPrimary()">Add Employee</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection    

@section('page-level-scripts')
    <script>
    count = 1
    function addContactTextField(id){
        count++;
        console.log(id);
        document.getElementById(id).innerHTML += 
                `<div class="col" id="col${id+count}">
                    <input type="text" id="${id+count}" name="contact[]"  class="c-secondary f-22 validate form-control">
                    <input type="radio" name = "radio" id = "${id+count}" value = "${id+count}" class="radio" onclick = "checked(${id+wcount})"> Primary 
                    <a class="btn-floating btn btn-danger pulse plus" id="${id+count}" onclick="deleted('col${id+count}')"><i class="fa fa-trash"></i></a> 
                </div>`;
            
        }

        ecount = 1;
        function addEmailTextField(id){
        ecount++;
        console.log(id);
        document.getElementById(id).innerHTML += 
                `<div class="col" id="col${id+ecount}">
                    <input type="text" id="${id+ecount}" name="email[]"  class="c-secondary f-22 validate form-control">
                    <input type="radio" name = "email_radio" id = "${id+ecount}" value = "${id+ecount}" class="radio" onclick = "checked(${id+wcount})"> Primary 
                    <a class="btn-floating btn btn-danger pulse plus" id="${id+ecount}" onclick="deleted('col${id+ecount}')"><i class="fa fa-trash"></i></a>  
                </div>`;
            
        }

        wcount = 1;
        function addWhatsappTextField(id){
        wcount++;
        console.log(id);
        document.getElementById(id).innerHTML += 
                `<div class="col" id="col${id+wcount}">
                    <input type="text" id="${id+wcount}" name="whatsapp[]"  class="c-secondary f-22 validate form-control">
                    <input type="radio" name = "wtsp_radio" id = "${id+wcount}" value = "${id+wcount}" class="radio" onclick = "checked(${id+wcount})"> Primary 
                    <a class="btn-floating btn btn-danger pulse plus" id="${id+wcount}" onclick="deleted('col${id+wcount}')"><i class="fa fa-trash"></i></a>  
                </div>`; 
        }
        function deleted(id) {
            document.getElementById(id).remove();
        }

        function setPrimary(id) {
            var radios = document.getElementsByTagName('input');
            var value;
            for (var i = 0; i < radios.length; i++) {
                if (radios[i].type === 'radio' && radios[i].checked) {
                    value = $("input[name='radio']:checked").prev().val();        
                    $(radios[i]).val(value);

                    value = $("input[name='email_radio']:checked").prev().val();        
                    $("input[name='email_radio']:checked").val(value);

                    value = $("input[name='wtsp_radio']:checked").prev().val();        
                    $("input[name='wtsp_radio']:checked").val(value);
                }
            }
        }

    </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous"></script>

    <script src="https://use.fontawesome.com/releases/v5.15.3/js/all.js" data-auto-replace-svg="nest"></script>

@endsection