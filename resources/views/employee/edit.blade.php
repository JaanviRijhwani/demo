@extends('layouts.app')

@section('page-level-styles')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5.15.3/css/fontawesome.min.css">
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><h3>Edit Employee</h3></div>
                        <div class="card-body">
                            <form action="{{ route('employee.update', $emp->id) }}" method="POST">
                                @csrf
                                @method('PUT')  
                                <div class="form-group">
                                    <label for="name">Employee Name</label>
                                    <input type="text"
                                        id="name"
                                        name="name"
                                        class="form-control {{ $errors->has('$emp->name') ? 'is-invalid' : '' }}" value="{{ old('name', $emp->name)}}"
                                        >
                                    @error('name')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                <h4>Address Details</h4>
                                <div class="row">
                                <div class="col">
                                <div class="form-group">
                                    <label for="address1">Address 1</label>
                                    <input type="text"
                                        id="address1"
                                        name="address1"
                                        class="form-control {{ $errors->has($emp->{'address 1'}) ? 'is-invalid' : '' }}" value="{{ old('name', $emp->{'address 1'})}}"
                                        >
                                    @error('address1')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                </div>
                                <div class="col">
                                <div class="form-group">
                                    <label for="address2">Address 2</label>
                                    <input type="text"
                                        id="address2"
                                        name="address2"
                                        class="form-control {{ $errors->has($emp->{'address 2'}) ? 'is-invalid' : '' }}" value="{{ old('name', $emp->{'address 2'})}}"
                                        >
                                    @error('address2')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                </div>

                                <div class="col">
                                <div class="form-group">
                                    <label for="location">Location</label>
                                    <input type="text"
                                        id="location"
                                        name="location"
                                        class="form-control {{ $errors->has($emp->location) ? 'is-invalid' : '' }}" value="{{ old('name', $emp->location)}}"
                                        >
                                    @error('location')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                </div>
                                </div>

                                <div class="row">
                                <div class="col">
                                <div class="form-group">
                                    <label for="zip">Zip/Postal Code</label>
                                    <input type="text"
                                        id="zip"
                                        name="zip"
                                        class="form-control {{ $errors->has($emp->zip) ? 'is-invalid' : '' }}" value="{{ old('name', $emp->zip)}}"
                                        >
                                    @error('zip')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                </div>
                                <div class="col">
                                <div class="form-group">
                                    <label for="postal_area">Postal Area</label>
                                    <input type="text"
                                        id="postal_area"
                                        name="postal_area"
                                        class="form-control {{ $errors->has($emp->{'postal area'}) ? 'is-invalid' : '' }}" value="{{ old('name', $emp->{'postal area'})}}"
                                        >
                                    @error('postal_area')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                </div>

                                <div class="col">
                                <div class="form-group">
                                    <label for="taluka">Taluka</label>
                                    <input type="text"
                                        id="taluka"
                                        name="taluka"
                                        class="form-control {{ $errors->has($emp->taluka) ? 'is-invalid' : '' }}" value="{{ old('name', $emp->taluka)}}"
                                        >
                                    @error('taluka')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                </div>
                                </div>


                                <div class="row">
                                <div class="col">
                                <div class="form-group">
                                    <label for="suburb">Suburb</label>
                                    <input type="text"
                                        id="suburb"
                                        name="suburb"
                                        class="form-control {{ $errors->has($emp->suburb) ? 'is-invalid' : '' }}" value="{{ old('name', $emp->suburb)}}"
                                        >
                                    @error('suburb')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                </div>
                                <div class="col">
                                <div class="form-group">
                                    <label for="east_west">East/West</label>
                                    <input type="text"
                                        id="east_west"
                                        name="east_west"
                                        class="form-control {{ $errors->has($emp->{'east/west'}) ? 'is-invalid' : '' }}" value="{{ old('name', $emp->{'east/west'})}}"
                                        >
                                    @error('east_west')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                </div>

                                <div class="col">
                                <div class="form-group">
                                    <label for="city">City</label>
                                    <input type="text"
                                        id="city"
                                        name="city"
                                        class="form-control {{ $errors->has($emp->city) ? 'is-invalid' : '' }}" value="{{ old('name', $emp->city)}}"
                                        >
                                    @error('city')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                </div>
                                </div>

                                <div class="row">
                                <div class="col">
                                <div class="form-group">
                                    <label for="district">District</label>
                                    <input type="text"
                                        id="district"
                                        name="district"
                                        class="form-control {{ $errors->has($emp->district) ? 'is-invalid' : '' }}" value="{{ old('name', $emp->district)}}"
                                        >
                                    @error('district')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                </div>
                                <div class="col">
                                <div class="form-group">
                                    <label for="state">State</label>
                                    <input type="text"
                                        id="state"
                                        name="state"
                                        class="form-control {{ $errors->has($emp->state) ? 'is-invalid' : '' }}" value="{{ old('name', $emp->state)}}"
                                        >
                                    @error('state')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                </div>

                                <div class="col">
                                <div class="form-group">
                                    <label for="country">Country</label>
                                    <input type="text"
                                        id="country"
                                        name="country"
                                        class="form-control {{ $errors->has($emp->country) ? 'is-invalid' : '' }}" value="{{ old('name', $emp->{'address 1'})}}"
                                        >
                                    @error('country')
                                        <div class="text-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                                </div>
                                </div>

                                <h4>Contact Details</h4>
                                <div class="row">
                                <div class="col">
                            <div class="row">
                                <div class="col" id="contact">
                                    <label for="contact1">Contact Number</label>
                                    <input type="hidden" value = "{{$id=1}}">
                                   @foreach($contact as $mob)
                                        
                                        <div id="colcontact{{$id}}">
                                        <input type="text"  id="contact{{$id}}" name="contact[]"  class="c-secondary f-22 validate form-control" value="{{$mob->mobile_number}}">
                                        <input type="radio" name="radio" id="contact{{$id}}" value="{{$mob->mobile_number}}" class="radio" {{ ($mob->is_primary_mobile == 1) ? 'checked'  :  '' }}> Primary  
                                        @if($id == 1)
                                            <a class="btn-floating btn btn-primary pulse plus" id="add-button-email" onclick ="addContactTextField('contact')">+</a>
                                        @endif
                                        @if($id != 1)
                                        
                                            <a class="btn-floating btn btn-danger pulse plus" id="" onclick="deleted('colcontact{{$id}}')"><i class="fa fa-trash"></i></a> 
                                        
                                        @endif
                                        </div>
                                        <input type="hidden" value="{{$id++}}">
                                   @endforeach
                                                                  
                                </div>
                                
                            </div>
                        </div>
                                
                                
                        <div class="col">
                            <div class="row">
                                <div class="col" id="whatsapp">
                                    <label for="whatsapp1">WhatsApp Number</label>
                                     <input type="hidden" value = "{{$id=1}}">
                                   @foreach($wtsp as $wp)
                                        <div id="colwhatsapp{{$id}}">
                                        <input type="text" id="whatsapp{{$id}}" name="whatsapp[]"  class="c-secondary f-22 validate form-control" value="{{$wp->mobile_number}}">
                                        <input type="radio" id="whatsapp{{$id}}" name="wtsp_radio" value="{{$wp->mobile_number}}" class="radio"  {{ ($wp->is_primary_wtsp == 1) ? 'checked'  :  '' }} > Primary  
                                        @if($id == 1)
                                            <a class="btn-floating btn btn-primary pulse plus" id="add-button-email" onclick ="addWhatsappTextField('whatsapp')">+</a>
                                        @endif
                                        @if($id != 1)
                                        
                                            <a class="btn-floating btn btn-danger pulse plus" id="" onclick="deleted('colwhatsapp{{$id}}')"><i class="fa fa-trash"></i></a> 
                                    
                                        @endif
                                        </div>
                                        <input type="hidden" value="{{$id++}}">
                                   @endforeach                               
                                </div>
                                
                                <div class="col ">
                                   
                                </div>
                            </div>
                        

                            </div>
                        <div class="col ">
                            <div class="row">
                                <div class="col s10" id = "email">
                                    <label for="email1">Email Address</label>
                                    <input type="hidden" value = "{{$id=1}}">
                                   @foreach($emails as $wp)
                                        <div id="colemail{{$id}}">
                                        <input type="text" id="email{{$id}}" name="email[]"  class="c-secondary f-22 validate form-control" value="{{$wp->email_id}}">
                                        <input type="radio" id="email{{$id}}" name="email_radio" value="{{$wp->email_id}}" class="radio"   {{ ($wp->is_primary == 1) ? 'checked'  :  '' }} > Primary  
                                        @if($id == 1)
                                            <a class="btn-floating btn btn-primary pulse plus" id="add-button-email" onclick ="addEmailTextField('email', {{$id}})">+</a>
                                        @endif
                                        @if($id != 1)
                                        
                                            <a class="btn-floating btn btn-danger pulse plus" id="" onclick="deleted('colemail{{$id}}')"><i class="fa fa-trash"></i></a> 
                                    
                                        @endif
                                        </div>
                                        <input type="hidden" value="{{$id++}}">
                                   @endforeach                               
                                </div>
                            </div>
                        </div>
                                </div>
                                </div>

                                
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary" onclick="setPrimary()">Edit Employee</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection    

@section('page-level-scripts')
    <script>
    
    function addContactTextField(id){
        count = document.getElementsByName('contact[]').length;
        console.log(count);
        count++;
        console.log(id);
        document.getElementById(id).innerHTML += 
                `<div class="col" id="col${id+count}">
                    <input type="text" id="${id+count}" name="contact[]"  class="c-secondary f-22 validate form-control">
                    <input type="radio" name = "radio" id = "${id+count}" value = "${id+count}" class="radio" onclick = "checked(${id+count})"> Primary 
                    <a class="btn-floating btn btn-danger pulse plus" id="${id+count}" onclick="deleted('col${id+count}')"><i class="fa fa-trash"></i></a> 
                </div>`;
            
        }

        
        function addEmailTextField(id){
            ecount = document.getElementsByName('email[]').length;
            console.log(ecount);
            ecount++;
            console.log(id);
            document.getElementById(id).innerHTML += 
                    `<div class="col" id="col${id+ecount}">
                        <input type="text" id="${id+ecount}" name="email[]"  class="c-secondary f-22 validate form-control">
                        <input type="radio" name = "email_radio" id = "${id+ecount}" value = "${id+ecount}" class="radio" onclick = "checked(${id+ecount})"> Primary 
                    <a class="btn-floating btn btn-danger pulse plus" id="${id+ecount}" onclick="deleted('col${id+ecount}')"><i class="fa fa-trash"></i></a>  
                </div>`;
            
        }

    
        function addWhatsappTextField(id){
            wcount = document.getElementsByName('whatsapp[]').length;
            wcount++;
            console.log(id);
            document.getElementById(id).innerHTML += 
                    `<div class="col" id="col${id+wcount}">
                        <input type="text" id="${id+wcount}" name="whatsapp[]"  class="c-secondary f-22 validate form-control">
                        <input type="radio" name = "wtsp_radio" id = "${id+wcount}" value = "${id+wcount}" class="radio" onclick = "checked(${id+wcount})"> Primary 
                        <a class="btn-floating btn btn-danger pulse plus" id="${id+wcount}" onclick="deleted('col${id+wcount}')"><i class="fa fa-trash"></i></a>  
                    </div>`; 
        }
        function deleted(id) {
            document.getElementById(id).remove();
        }

        function setPrimary(id) {
            var radios = document.getElementsByTagName('input');
            var value;
            for (var i = 0; i < radios.length; i++) {
                if (radios[i].type === 'radio' && radios[i].checked) {
                    value = $("input[name='radio']:checked").prev().val();        
                    $(radios[i]).val(value);

                    value = $("input[name='email_radio']:checked").prev().val();        
                    $("input[name='email_radio']:checked").val(value);

                    value = $("input[name='wtsp_radio']:checked").prev().val();        
                    $("input[name='wtsp_radio']:checked").val(value);
                }
            }
        }

    </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous"></script>

    <script src="https://use.fontawesome.com/releases/v5.15.3/js/all.js" data-auto-replace-svg="nest"></script>

@endsection