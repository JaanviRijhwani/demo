<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateEmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:100',
            'address1' => 'required',
            'address2' => 'required',
            'location' => 'required',
            'zip' => 'required',
            'postal_area' => 'required',
            'location' => 'required',
            'taluka'=>'required',
            'suburb' => 'required',
            'east_west' => 'required',
            'city' => 'required',
            'district' => 'required',
            'state' => 'required',
            'country' => 'required'
        ];
    }
}
