<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddEmployeeRequest;
use App\Http\Requests\UpdateEmployeeRequest;
use App\Models\Address;
use App\Models\Employee;
use App\Models\Email;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    public function index()
    {
        
        
        // return view('/employee.index', compact([
        //     'employees'
        // ]));
        // redirect('employee.add');
        
        $employees = Employee::getEmployees();
        // dd($employees);
        return view('employee.index', [
            'employees' => $employees
        ]);
    }

    public function create()
    {
        return view('employee.add');
    }

    public function store(AddEmployeeRequest $request) {
        // dd($request);s
       
        $address = Address::create([
            'address 1' => $request->address1,
            'address 2' => $request->address2,
            'location' => $request->location,
            'zip' => $request->zip,
            'postal area' => $request->postal_area,
            'country' => $request->country,
            'state' => $request->state,
            'city' => $request->city,
            'district' => $request->district,
            'suburb' => $request->suburb,
            'taluka' => $request->taluka,
            'east/west' => $request->east_west
        ]);

        // dd($address->id);
        $emp = Employee::create([
            'name' => $request->name,
            'address_id' => $address->id
        ]);


        // dd($address->id);
        // dd($request->email); 
        for ($i = 0; $i < count($request->email); $i++)   
        {
            $email = \DB::table('emails')->insertGetId([
                'email_id' => $request->email[$i],
                'is_primary' => ($request->email[$i] === $request->email_radio)? 1 : 0,
                'employee_id' => $emp->id
            ]);
        }


        for ($i = 0; $i < count($request->whatsapp); $i++) {
            $arr = [];
            array_push($arr, $request->whatsapp[$i]);
           
        }

        for ($i = 0; $i < count($request->contact); $i++) {
            if (!(in_array($request->contact[$i], $request->whatsapp))) {
                array_push($arr, $request->contact[$i]);
            }
        }

        // dd( $request);

        $var = 0;
        for ($i = 0; $i < count($arr); $i++) {
            if ((in_array($arr[$i], $request->whatsapp))) {
                $var = 1;
            } else{
                $var = 0;
            }
            $number = \DB::table('mobile_number_whatsapp_numbers')->insertGetId([
                'mobile_number' => $arr[$i],
                'is_primary_mobile' => ($arr[$i] == $request->radio)? 1 : 0,
                'is_primary_wtsp' => ($arr[$i] == $request->wtsp_radio)? 1 : 0,
                'is_whatsapp' => $var,
                'employee_id' => $emp->id
            ]);
            
        }
        // dd($emp->id);

        $employees = Employee::getEmployees();
        return view('employee.index', compact([
            'employees'
        ]));

        
        
    }

    public function edit(int $id)
    {
        $emp = Employee::getEmployee($id);
        $contact = Employee::getContact($id);
        $wtsp = Employee::getWhatsapp($id);
        $emails = Employee::getEmail($id);
        return view('employee.edit', compact(['emp', 'contact', 'wtsp', 'emails']));
    }

    public function update(UpdateEmployeeRequest $request, int $id)
    {
        // dd($request->contact);
        
        $emp = \DB::table('employees')->where('id', $id)->update(['name' => $request->name]);
        $address_id = \DB::table('employees')->select("address_id")->where('id', $id)->get('address_id');
        // dd($request);
        $address = \DB::table('addresses')->where('id', $address_id[0]->address_id)->update([
            'address 1' => $request->address1,
            'address 2' => $request->address2,
            'location' => $request->location,
            'zip' => $request->zip,
            'postal area' => $request->postal_area,
            'country' => $request->country,
            'state' => $request->state,
            'city' => $request->city,
            'district' => $request->district,
            'suburb' => $request->suburb,
            'taluka' => $request->taluka,
            'east/west' => $request->east_west]);
        
        $contact = Employee::getContact($id);
        // $contact->sync($request->contact);
        // dd($contact);
        // dd($request->contact);
        $update_arr = [];
        $delete_arr = [];
        $create_arr = [];
        // dd($contact[0]->id);
        for ($i = 0; $i < count($request->contact); $i++) {
            
            for ($j = 0; $j < count($contact); $j++)
            {
                if (((int)$request->contact[$i]) == $contact[$j]->mobile_number && !in_array($contact[$i]->mobile_number, $update_arr)) {
                    array_push($update_arr, $contact[$i]->mobile_number);
                }
            }
        }
        // dd($update_arr);
        for ($i = 0; $i < count($request->contact); $i++) {

            for ($j = 0; $j < count($contact); $j++)
            {   
                // var_dump("for: " . $request->contact[$i]);
                // var_dump(in_array((int)$request->contact[$i], $update_arr)."===================");
                // var_dump(in_array((int)$request->contact[$i], $create_arr));
                if(((!  in_array((int)$request->contact[$i], $update_arr)) &&  (! in_array((int)$request->contact[$i], $create_arr))))
                {
                    array_push($create_arr, $request->contact[$i]);
                }
            }
        }
        
        for ($j = 0; $j < count($contact); $j++)
        {
            if (!(in_array($contact[$j]->id, $update_arr))) {
                array_push($delete_arr, $contact[$j]->mobile_number);
            }
        }


        // dd($create_arr);

        for ($j = 0; $j < count($delete_arr); $j++)
        {
            $del = \DB::table('mobile_number_whatsapp_numbers')->where('id', $delete_arr[$j])->delete();
        }
       
        for ($i = 0; $i < count($create_arr); $i++) {
            $number = \DB::table('mobile_number_whatsapp_numbers')->insertGetId([
                'mobile_number' => $create_arr[$i],
                'is_primary_mobile' => ($create_arr[$i] == $request->radio)? 1 : 0,
                'is_primary_wtsp' => 0,
                'is_whatsapp' => 0,
                'employee_id' => $id
            ]);
            // dd($number);
        }
        // dd($create_arr);

        for ($i = 0; $i < count($update_arr); $i++) {
            $no = \DB::table('mobile_number_whatsapp_numbers')->where('mobile_number', $update_arr[$i])->update([
                'is_primary_mobile' => ($update_arr[$i] == $request->radio)? 1 : 0,
                'is_primary_wtsp' => 0,
                'is_whatsapp' => 0,
                'employee_id' => $id
            ]);
            // dd($no);
        }

        // var_dump("u",$update_arr);
        // var_dump("c",$create_arr );
        // var_dump("del",$delete_arr);
        // dd("h");

        //wtsp
        $wtsp = Employee::getWhatsapp($id);
        $update_arr = [];
        $delete_arr = [];
        $create_arr = [];

        for ($i = 0; $i < count($request->whatsapp); $i++) {
            
            for ($j = 0; $j < count($wtsp); $j++)
            {
                if (((int)$request->whatsapp[$i]) == $wtsp[$j]->mobile_number) {
                    array_push($update_arr, $wtsp[$i]->mobile_number);
                }
            }
        }
        
        for ($i = 0; $i < count($request->whatsapp); $i++) {

            for ($j = 0; $j < count($wtsp); $j++)
            {   
                if(! in_array($request->whatsapp[$i], $update_arr) &&  ! in_array($request->whatsapp[$i], $create_arr))
                {
                    array_push($create_arr, $request->whatsapp[$i]);
                }
            }
        }
        
        for ($j = 0; $j < count($wtsp); $j++)
        {
            if (!(in_array($wtsp[$j]->id, $update_arr))) {
                array_push($delete_arr, $wtsp[$j]->mobile_number);
            }
        }
        // dd($delete_arr);
        
        for ($j = 0; $j < count($delete_arr); $j++)
        {
            $del = \DB::table('mobile_number_whatsapp_numbers')->where('id', $delete_arr[$j])->delete();
        }

        
       
        for ($i = 0; $i < count($create_arr); $i++) {
            $n = \DB::table('mobile_number_whatsapp_numbers')->insertGetId([
                'mobile_number' => $create_arr[$i],
                'is_primary_mobile' => 0,
                'is_primary_wtsp' => ($create_arr[$i] == $request->wtsp_radio)? 1 : 0,
                'is_whatsapp' => 1,
                'employee_id' => $id
            ]);
        }

        for ($i = 0; $i < count($update_arr); $i++) {
            $num = \DB::table('mobile_number_whatsapp_numbers')->where('mobile_number', $update_arr[$i])->update([
                'is_primary_mobile' => 0,
                'is_primary_wtsp' => ($update_arr[$i] == $request->wtsp_radio)? 1 : 0,
                'is_whatsapp' => 1,
                'employee_id' => $id
            ]);
        }

        //email
        $emails = Employee::getEmail($id);
        $update_arr = [];
        $delete_arr = [];
        $create_arr = [];
        // // dd($request);
        for ($i = 0; $i < count($request->email); $i++) {
            
            for ($j = 0; $j < count($emails); $j++)
            {
                if (($request->email[$i]) == $emails[$j]->email_id) {
                    array_push($update_arr, $emails[$i]->email_id);
                }
            }
        }
        
        for ($i = 0; $i < count($request->email); $i++) {

        for ($j = 0; $j < count($emails); $j++)
        {   
            if(! in_array($request->email[$i], $update_arr) &&  ! in_array($request->email[$i], $create_arr))
            {
                array_push($create_arr, $request->email[$i]);
            }
        }
    }
    for ($j = 0; $j < count($emails); $j++)
    {
        if (!(in_array($emails[$j]->email_id, $update_arr))) {
            array_push($delete_arr, $emails[$j]->id);
        }
    }

        for ($j = 0; $j < count($delete_arr); $j++)
        {
            $del = \DB::table('emails')->where('id', $delete_arr[$j])->delete();
        }

        for ($i = 0; $i < count($create_arr); $i++)   
        {
            $email = \DB::table('emails')->insertGetId([
                'email_id' => $create_arr[$i],
                'is_primary' => ($create_arr[$i] == $request->email_radio)? 1 : 0,
                'employee_id' => $id
            ]);
        }

        for ($i = 0; $i < count($update_arr); $i++) {
            $emails = \DB::table('emails')->where('id', $update_arr[$i])->update([
                'email_id' => $update_arr[$i],
                'is_primary' => ($update_arr[$i] == $request->email_radio)? 1 : 0,
                'employee_id' => $id
            ]);
        }

    }

    public function destroy(int $id)
    {
        // print_r("DEL CAUGHT ME!:(" . $id);
        // dd($id);
        Employee::deleteEmployee($id);

        $employees = Employee::getEmployees();
        return view('employee.index', compact([
            'employees'
        ]));

    }
}
