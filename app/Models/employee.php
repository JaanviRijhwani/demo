<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'address_id'
    ];

    public static function getEmployees() {
        $emp = \DB::select(\DB::raw("SELECT e.id, e.name, a.state, a.city FROM employees as e INNER JOIN addresses as a on e.address_id = a.id group by e.id"));
        return $emp;
    }

    public static function deleteEmployee(int $id) {
        $del = \DB::delete(\DB::raw("DELETE e, a, em FROM employees as e INNER JOIN addresses as a on e.address_id = a.id INNER JOIN emails as em on em.employee_id = e.id WHERE e.id = $id"));
        return $del;
    }

    public static function getEmployee(int $id) {
        $emp = \DB::select(\DB::raw("SELECT * FROM employees as e INNER JOIN addresses as a on  a.id = e.address_id  WHERE e.id = $id"))[0];
        return $emp;
    }

    public static function getContact(int $id){
        $contact = \DB::select(\DB::raw("SELECT * FROM mobile_number_whatsapp_numbers WHERE employee_id = $id AND is_whatsapp = 0 AND is_primary_wtsp = 0"));
        // dd($contact);
        return $contact;
    }

    public static function getWhatsapp(int $id) {
        $wtsp = \DB::select(\DB::raw("SELECT * FROM `mobile_number_whatsapp_numbers` WHERE employee_id = $id AND is_primary_mobile = 0 AND is_whatsapp = 1"));
        // dd($wtsp);
        return $wtsp;
    }

    public static function getEmail(int $id) {
        $email = \DB::select(\DB::raw("SELECT * FROM emails WHERE employee_id = $id"));
        // dd($email); 
        return $email;
    }

}
