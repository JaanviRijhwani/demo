<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    use HasFactory;

    protected $fillable = [
        'address 1',
        'address 2',
        'location',
        'zip',
        'postal area',
        'location',
        'taluka',
        'suburb',
        'east/west',
        'city',
        'district',
        'state',
        'country'
    ];
}
