<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MobileWhatsAppNumber extends Model
{
    use HasFactory;

    protected $fillable = [
        'mobile_number',
        'is_primary_mobile',
        'is_primary_wtsp',
        'is_whatsapp',
        'employee_id'
    ];
}
